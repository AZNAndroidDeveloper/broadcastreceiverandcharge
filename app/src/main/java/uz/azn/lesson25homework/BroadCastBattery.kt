package uz.azn.lesson25homework

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager


class BroadCastBattery(val battery:BatteryInterface) :BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) {
      val  level = intent?.getIntExtra(BatteryManager.EXTRA_LEVEL,-1)!!
        battery.battery(level)
    }


//
//    Log.d("TAG", "onReceive: $level")



}