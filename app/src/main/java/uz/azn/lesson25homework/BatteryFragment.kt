package uz.azn.lesson25homework

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import uz.azn.lesson25homework.databinding.FragmentBatteryBinding
import kotlin.math.log


class BatteryFragment : Fragment(R.layout.fragment_battery),BatteryInterface {
    lateinit var binding: FragmentBatteryBinding

    lateinit var batteryBroadcast: BroadCastBattery
    lateinit var internetBroadcast: NetworkingBroadCast
//    val batteryBroadcast: BatteryBroadcast = BatteryBroadcast()
//    val internetBroadcast: InternetBroadcast = InternetBroadcast()




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentBatteryBinding.bind(view)
        internetBroadcast = NetworkingBroadCast(this)
        batteryBroadcast = BroadCastBattery(this)
        activity?.registerReceiver(batteryBroadcast, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        activity?.registerReceiver(internetBroadcast, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))



//        val battery = batteryBroadcast.battery()
//
//        val network = internetBroadcast.network()



//
//        binding.progress.progress = battery
//        binding.internetText.text = network
       // Log.d("BatteryFragment", "onViewCreated: $battery $network")

    }


    override fun onDestroyView() {
        super.onDestroyView()
        activity?.unregisterReceiver(batteryBroadcast)
        activity?.unregisterReceiver(internetBroadcast)
    }

    override fun battery(a: Int) {
        binding.progress.progress = a
        binding.batteryText.text="$a %"
    }

    override fun online(text: String) {
        binding.internetText.text = text
    }


//    inner class BatteryBroadcast : BroadcastReceiver() {
//        override fun onReceive(context: Context?, intent: Intent?) {
//            val level = intent?.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
//            binding?.batteryText?.text = "$level%"
//            binding?.progress?.progress = level!!
//        }
//
//    }

//    inner class InternetBroadcast : BroadcastReceiver() {
//        override fun onReceive(context: Context?, intent: Intent?) {
//            if (isOnline(context)) {
//                binding?.internetText?.text = "Online"
//            } else {
//                binding?.internetText?.text = "Offline"
//            }
//        }
//
//    }
//
//    fun isOnline(context: Context?): Boolean {
//        val connectivityManager =
//            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//        val activeNetworkInfo = connectivityManager.activeNetworkInfo
//
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected
//
//    }
}



