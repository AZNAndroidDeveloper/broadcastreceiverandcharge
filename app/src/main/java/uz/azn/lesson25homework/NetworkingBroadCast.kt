package uz.azn.lesson25homework

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log

class NetworkingBroadCast(val internet:BatteryInterface):BroadcastReceiver() {
    var text = ""
    override fun onReceive(context: Context?, intent: Intent?) {
        if (isOnline(context)) text = "Online"
        else text = "Offline"

        internet.online(text)


    }
    fun isOnline(context: Context?):Boolean{
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo!=null && activeNetworkInfo.isConnected
    }


}
