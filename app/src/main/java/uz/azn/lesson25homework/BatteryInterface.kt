package uz.azn.lesson25homework

interface BatteryInterface {
    fun battery(a:Int)
    fun online(text:String)
}